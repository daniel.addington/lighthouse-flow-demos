import fs from 'fs';
import open from 'open';
import puppeteer from 'puppeteer';
import {startFlow} from 'lighthouse/lighthouse-core/fraggle-rock/api.js';

// Desktop config - https://github.com/GoogleChrome/lighthouse/discussions/13320
import { default as config } from 'lighthouse/lighthouse-core/config/desktop-config.js';

async function captureReport() {
    const browser = await puppeteer.launch({headless: false});
    const page = await browser.newPage();
  
    const flow = await startFlow(page, {config, name: 'Product stripe flow snapshots'});
  
    await page.goto('https://www.cali.test.cutely.xyz/a',
        { waitUntil: ['load', 'domcontentloaded']/*'networkidle0'*/
    });
  
    // Wait for search button
    const searchBoxSelector = 'input.tm-global-search__search-input-box';
    await page.waitForSelector(searchBoxSelector);
    await flow.snapshot({stepName: 'Homepage loaded'});

    // Type iPhone 12
    await page.type(searchBoxSelector, 'iphone 12', {delay: 200})
    await page.click('button.tm-global-search__search-form-submit-button');
  
    // Wait for stripe in UI.
    const productStripeSelector = 'div.tm-product-summary-card-content';
    await page.waitForSelector(productStripeSelector);
    await flow.snapshot({stepName: 'Stripe loaded'});
      
    browser.close();
  
    const report = flow.generateReport();
    fs.writeFileSync('flow.report.html', report);
    open('flow.report.html', {wait: false});
  }
  
  captureReport();