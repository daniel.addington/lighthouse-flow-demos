import fs from 'fs';
import open from 'open';
import puppeteer from 'puppeteer';
import {startFlow} from 'lighthouse/lighthouse-core/fraggle-rock/api.js';

// Desktop config - https://github.com/GoogleChrome/lighthouse/discussions/13320
import { default as config } from 'lighthouse/lighthouse-core/config/desktop-config.js';

async function captureReport() {
  const browser = await puppeteer.launch({headless: false});
  const page = await browser.newPage();

  const flow = await startFlow(page, {config, name: 'Single Navigation'});
  await flow.navigate('https://www.trademe.co.nz/a/');

  await browser.close();

  const report = flow.generateReport();
  fs.writeFileSync('flow.report.html', report);
  open('flow.report.html', {wait: false});
}

captureReport();