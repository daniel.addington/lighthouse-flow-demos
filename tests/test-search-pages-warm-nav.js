import fs from 'fs';
import open from 'open';
import puppeteer from 'puppeteer';
import {startFlow} from 'lighthouse/lighthouse-core/fraggle-rock/api.js';

const ADS_DISABLED = true;

async function captureReport() {
    const browser = await puppeteer.launch({headless: true});
    const page = await browser.newPage();

    const locationToTest = [
      { url: 'https://www.daddington.test.cutely.app/a/', description: 'Home'},
      { url: 'https://www.daddington.test.cutely.app/a/motors/search/', description: 'Motors Search'},
      { url: 'https://www.daddington.test.cutely.app/a/property/search', description: 'Property Search'},
      { url: 'https://www.daddington.test.cutely.app/a/search/', description: 'Main Search'},
      { url: 'https://www.daddington.test.cutely.app/a/jobs/search/', description: 'Jobs Search'}
    ]
  
    const flow = await startFlow(page, {name: 'Cold and warm navigations'});

    for (let location of locationToTest) {
      await navigateToUrl(flow, location.url, location.description);
    }

    await browser.close();
  
    const report = flow.generateReport();
    fs.writeFileSync('flow.report.html', report);
    open('flow.report.html', {wait: false});
  }

  async function navigateToUrl(flow, url, description) {
    if (ADS_DISABLED) {
      url = url + '?noAds='
    }
    await flow.navigate(url, {
      stepName: description + ' - cold navigation'
    });
    await flow.navigate(url, {
      stepName: description + ' - warm navigation',
      configContext: {
        settingsOverrides: {disableStorageReset: true},
      },
    });
  }
  
  captureReport();