import fs from 'fs';
import open from 'open';
import puppeteer from 'puppeteer';
import {startFlow} from 'lighthouse/lighthouse-core/fraggle-rock/api.js';

async function captureReport() {
    const browser = await puppeteer.launch({headless: false});
    const page = await browser.newPage();
  
    const flow = await startFlow(page, {name: 'Motors flow snapshots'});
  
    await page.goto('https://www.trademe.co.nz/a/',
        { waitUntil: ['load', 'domcontentloaded']/*'networkidle0'*/
    });
  
    // Wait for motors button, then click it.
    const motorsLinkSelector = 'a.tm-homepage-search-header__vertical-links-link--motors';
    await page.waitForSelector(motorsLinkSelector);
    await flow.snapshot({stepName: 'Homepage loaded'});
    await page.click(motorsLinkSelector);
  
    // Wait for more options button in UI, then open it.
    const moreButtonSelector = 'button.tm-motors-search-bar__toggle-button';
    await page.waitForSelector(moreButtonSelector);
    await flow.snapshot({stepName: 'Motors homepage loaded'});
    await page.click(moreButtonSelector);
  
    await flow.snapshot({stepName: 'Advanced settings opened'});
  
    browser.close();
  
    const report = flow.generateReport();
    fs.writeFileSync('flow.report.html', report);
    open('flow.report.html', {wait: false});
  }
  
  captureReport();