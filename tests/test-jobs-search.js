import fs from 'fs';
import open from 'open';
import puppeteer from 'puppeteer';
import {startFlow} from 'lighthouse/lighthouse-core/fraggle-rock/api.js';

// Desktop config - https://github.com/GoogleChrome/lighthouse/discussions/13320
import { default as config } from 'lighthouse/lighthouse-core/config/desktop-config.js';

// Inspiration for this script here:
// https://lzomedia.com/blog/getting-started-with-lighthouse-user-flows/
// https://developer.chrome.com/docs/devtools/recorder/

(async () => {
    const browser = await puppeteer.launch({headless: false});
    const page = await browser.newPage();
    const flow = await startFlow(page, { config, name: 'Job Search Flow' });

    async function waitForSelectors(selectors, frame) {
      for (const selector of selectors) {
        try {
          return await waitForSelector(selector, frame);
        } catch (err) {
          console.error(err);
        }
      }
      throw new Error('Could not find element for selectors: ' + JSON.stringify(selectors));
    }

    async function waitForSelector(selector, frame) {
      if (selector instanceof Array) {
        let element = null;
        for (const part of selector) {
          if (!element) {
            element = await frame.waitForSelector(part);
          } else {
            element = await element.$(part);
          }
          if (!element) {
            throw new Error('Could not find element: ' + part);
          }
          element = (await element.evaluateHandle(el => el.shadowRoot ? el.shadowRoot : el)).asElement();
        }
        if (!element) {
          throw new Error('Could not find element: ' + selector.join('|'));
        }
        return element;
      }
      const element = await frame.waitForSelector(selector);
      if (!element) {
        throw new Error('Could not find element: ' + selector);
      }
      return element;
    }

    async function waitForElement(step, frame) {
      const count = step.count || 1;
      const operator = step.operator || '>=';
      const comp = {
        '==': (a, b) => a === b,
        '>=': (a, b) => a >= b,
        '<=': (a, b) => a <= b,
      };
      const compFn = comp[operator];
      await waitForFunction(async () => {
        const elements = await querySelectorsAll(step.selectors, frame);
        return compFn(elements.length, count);
      });
    }

    async function querySelectorsAll(selectors, frame) {
      for (const selector of selectors) {
        const result = await querySelectorAll(selector, frame);
        if (result.length) {
          return result;
        }
      }
      return [];
    }

    async function querySelectorAll(selector, frame) {
      if (selector instanceof Array) {
        let elements = [];
        let i = 0;
        for (const part of selector) {
          if (i === 0) {
            elements = await frame.$$(part);
          } else {
            const tmpElements = elements;
            elements = [];
            for (const el of tmpElements) {
              elements.push(...(await el.$$(part)));
            }
          }
          if (elements.length === 0) {
            return [];
          }
          const tmpElements = [];
          for (const el of elements) {
            const newEl = (await el.evaluateHandle(el => el.shadowRoot ? el.shadowRoot : el)).asElement();
            if (newEl) {
              tmpElements.push(newEl);
            }
          }
          elements = tmpElements;
          i++;
        }
        return elements;
      }
      const element = await frame.$$(selector);
      if (!element) {
        throw new Error('Could not find element: ' + selector);
      }
      return element;
    }

    async function waitForFunction(fn) {
      let isActive = true;
      setTimeout(() => {
        isActive = false;
      }, 5000);
      while (isActive) {
        const result = await fn();
        if (result) {
          return;
        }
        await new Promise(resolve => setTimeout(resolve, 100));
      }
      throw new Error('Timed out');
    }
    {
        const targetPage = page;
        await targetPage.setViewport({"width":1903,"height":585})
    }
    {
        // const targetPage = page;
        // const promises = [];
        // promises.push(targetPage.waitForNavigation());
        // await targetPage.goto('https://www.daddington.test.cutely.app/a/jobs');
        await flow.navigate('https://www.daddington.test.cutely.app/a/jobs/', {
          stepName: 'Jobs navigation'
        });
        // await Promise.all(promises);
    }
    {
        const targetPage = page;
        const element = await waitForSelectors([["body > tm-root > div:nth-child(1) > main > div > tm-jobs-home-page > tm-home-page-header > div > tm-jobs-search-bar > div > div.tm-jobs-search-bar__content-container > tm-jobs-search-form > form > div:nth-child(1) > div.tm-jobs-search-form__category-select-group > tm-jobs-homepage-filter > div > button > span"]], targetPage);
        await element.click({ offset: { x: 53.203125, y: 29.1875} });
    }
    {
      await flow.snapshot({ stepName: 'Step 1' });
    }
    {
        const targetPage = page;
        const element = await waitForSelectors([["tg-scrollable-container > div > div > tm-multi-category-refiner > div > tg-rack > div:nth-child(3) > label > tg-rack-item > div > div > tg-rack-item-primary > div > span"]], targetPage);
        await element.click({ offset: { x: 56.65625, y: 6.1875} });
    }
    {
      await flow.snapshot({ stepName: 'Step 2' });
    }
    {
        const targetPage = page;
        const element = await waitForSelectors([["tg-scrollable-container > div > div > tm-multi-category-refiner > div > tg-rack > div:nth-child(2) > label > tg-rack-item > div > div > tg-rack-item-primary > div"]], targetPage);
        await element.click({ offset: { x: 28.109375, y: 10.1875} });
    }
    {
      await flow.snapshot({ stepName: 'Step 3' });
    }
    {
        const targetPage = page;
        const element = await waitForSelectors([["body > tm-root > div:nth-child(1) > main > div > tm-jobs-home-page > tm-home-page-header > div > tm-jobs-search-bar > div > div.tm-jobs-search-bar__content-container > tm-jobs-search-form > form > div.tm-jobs-search-form__search-footer > div.tm-jobs-search-form__sub-search-footer"]], targetPage);
        await element.click({ offset: { x: 32.890625, y: 10.1875} });
    }
    {
        const targetPage = page;
        const element = await waitForSelectors([["aria/Search jobs"],["body > tm-root > div:nth-child(1) > main > div > tm-jobs-home-page > tm-home-page-header > div > tm-jobs-search-bar > div > div.tm-jobs-search-bar__content-container > tm-jobs-search-form > form > div.tm-jobs-search-form__search-footer > div.tm-jobs-search-form__sub-search-footer > div.tm-jobs-search-form__search-button-container > button"]], targetPage);
        await element.click({ offset: { x: 46.421875, y: 12.1875} });
    }
    {
      await flow.snapshot({ stepName: 'Step 4' });
    }
    {
        const targetPage = page;
        await targetPage.evaluate((x, y) => { window.scroll(x, y); }, 0, 100)
    }
    {
        const targetPage = page;
        const element = await waitForSelectors([["body > tm-root > div:nth-child(1) > main > div > tm-jobs-search-results > tm-search-header > div > div > tm-refine-header > div:nth-child(4) > tm-tags > tm-drop-down-tag:nth-child(3) > tg-tag > span > button > span"]], targetPage);
        await element.click({ offset: { x: 61.34375, y: 17.8125} });
    }
    {
      await flow.snapshot({ stepName: 'Step 5' });
    }
    {
        const targetPage = page;
        const element = await waitForSelectors([["aria/Region"],["#tg-194"]], targetPage);
        await element.click({ offset: { x: 101.34375, y: 12.8125} });
    }
    {
      await flow.snapshot({ stepName: 'Step 6' });
    }
    {
        const targetPage = page;
        const element = await waitForSelectors([["aria/Region"],["#tg-194"]], targetPage);
        const type = await element.evaluate(el => el.type);
        if (["textarea","select-one","text","url","tel","search","password","number","email"].includes(type)) {
          await element.type('6: Object');
        } else {
          await element.focus();
          await element.evaluate((el, value) => {
            el.value = value;
            el.dispatchEvent(new Event('input', { bubbles: true }));
            el.dispatchEvent(new Event('change', { bubbles: true }));
          }, "6: Object");
        }
    }
    {
        const targetPage = page;
        const element = await waitForSelectors([["div.tm-tags__refiner-dropdown-content > tg-scrollable-container > div > div"]], targetPage);
        await element.evaluate((el, x, y) => { el.scrollTop = y; el.scrollLeft = x; }, 0, 100);
    }
    {
        const targetPage = page;
        const element = await waitForSelectors([["body > tm-root > div:nth-child(1) > main > div > tm-jobs-search-results > tm-search-header > div > div > tm-refine-header > div:nth-child(4) > tm-tags > tm-drop-down-tag:nth-child(4) > tg-tag > span > button > span"]], targetPage);
        await element.click({ offset: { x: 97.1875, y: 18.8125} });
    }
    {
      await flow.snapshot({ stepName: 'Step 7' });
    }
    {
        const targetPage = page;
        const element = await waitForSelectors([["div.tm-tags__refiner-dropdown-content > tg-scrollable-container > div > div > tm-refiner-switcher > tm-radio-refiner > tg-radio-group > fieldset > tg-radio-item:nth-child(3) > label > div > div > span"]], targetPage);
        await element.click({ offset: { x: 20.1875, y: 14.8125} });
    }
    {
      await flow.snapshot({ stepName: 'Step 8' });
    }
    {
        const targetPage = page;
        const element = await waitForSelectors([["body > tm-root > div:nth-child(1) > main > div > tm-jobs-search-results > tm-search-header > div"]], targetPage);
        await element.click({ offset: { x: 13.5, y: 263} });
    }
    {
      await flow.snapshot({ stepName: 'Step 9' });
    }

    await browser.close();

    const report = flow.generateReport();
    fs.writeFileSync('flow.report.html', report);
    open('flow.report.html', {wait: false});
})();
