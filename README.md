## Summary
Collection of scripts demonstrating behaviours of the new Lighthouse flow apis.

See: https://web.dev/lighthouse-user-flows/

## Instructions
* Ensure you have node/npm installed
* Run `npm install`
* Run `node <script name>` E.g. `node tests/test-simple-navigation.js`
